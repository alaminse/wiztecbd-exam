<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
class PostController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();

        return view('dashboard', compact('posts'));
    }
    public function create()
    {
        return view('post.create');
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'max:255'],
            'content' => ['required'],
            'published_at' => ['date_format:Y-m-d\TH:i'],
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $validate = $validator->validated();
        $validate['slug'] = Str::slug($validate['title']);
        $validate['user_id'] = auth()->user()->id;

        if ($request->hasFile('image')) {
            $imagePath = $request->file('image')->store('images/posts', 'public');
            $validate['image'] = $imagePath;
        }

        $done = Post::create($validate);

        if(!$done) return redirect()->back()->with('error', 'Post not create. Try Again!!');
        return redirect()->route('posts.index')->with('success', 'Post create Successfully!');
    }
    public function edit(Post $post)
    {
        return view('post.edit', compact('post'));
    }
    public function show(Post $post)
    {
        return view('post.show', compact('post'));
    }
    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required', 'max:255'],
            'content' => ['required'],
            'published_at' => ['date_format:Y-m-d\TH:i'],
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $validate = $validator->validated();
        $validate['slug'] = Str::slug($validate['title']);
        $validate['user_id'] = auth()->user()->id;

        if ($request->hasFile('image')) {
            if ($post->image) {
                Storage::disk('public')->delete($post->image);
            }
            
            $imagePath = $request->file('image')->store('images/posts', 'public');
            $validate['image'] = $imagePath;
        }

        $done = $post->update($validate);

        if(!$done) return redirect()->back()->with('error', 'Post not update. Try Again!!');
        return redirect()->route('posts.index')->with('success', 'Post update Successfully!');
    }

    public function destroy(Post $post)
    {
        if($post->delete())
        {
            return redirect()->route('posts.index')->with('success', 'Post deleted Successfully.');
        }
        return redirect()->back()->with('error', 'Post not deleted. Try Again!!');
    }
}
