<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post Details') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                        <div class="mb-3">
                            <label for="title" class="form-label"><strong>Title:</strong> </label> {{$post->title}}
                        </div>

                        <div class="mb-3">
                            <label for="published_at" class="form-label"><strong>Published At:</strong> </label>{{ $post->published_at }}
                        </div>

                        <div class="mb-3">
                            <label for="content" class="form-label"><strong>Content:</strong></label>
                            <br>
                            {{ $post->content }}
                        </div>
                        <img src="{{ asset('storage/' . $post->image) }}" alt="Post Image" height="300px" width="750px">

                        <a href="{{route('posts.index')}}" class="btn btn-info">Back</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
