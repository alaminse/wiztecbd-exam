<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="d-flex mb-3">
                        <div class="p-2"></div>
                        <div class="ms-auto p-2">
                            <a class="btn btn-primary" href="{{ route('posts.create') }}">Post Create</a>
                        </div>
                    </div>
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('success') }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Image</th>
                            <th scope="col">Content</th>
                            <th scope="col">Published Date</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $key => $post)
                            <tr>
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>{{$post->title}}</td>
                                <td>
                                    <img src="{{ asset('storage/' . $post->image) }}" alt="Post Image" height="50px" width="150px">
                                </td>
                                <td>{{ Str::limit($post->content, $limit = 30, $end = '...') }}</td>
                                <td>{{$post->published_at}}</td>
                                <td>
                                    <a class="btn btn-warning" href="{{route('posts.edit', $post->id)}}">Edit</a>
                                    <a class="btn btn-info" href="{{route('posts.show', $post->id)}}">Show</a>
                                    <a class="btn btn-danger" onclick="confirmDelete()">Delete</a>
                                    <form action="{{ route('posts.destroy', $post->id) }}" method="POST" id="deleteForm">
                                        @csrf
                                        @method('DELETE')

                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- SweetAlert CDN -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>
        function confirmDelete() {

            Swal.fire({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    document.getElementById('deleteForm').submit();

                    Swal.fire(
                        'Deleted!',
                        'Your record has been deleted.',
                        'success'
                    );
                }
            });
        }
    </script>
</x-app-layout>
