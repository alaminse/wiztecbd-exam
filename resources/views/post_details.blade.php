<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Post Details') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    <div class="d-flex mb-3">
                        <div class="p-2">
                            <h2><strong>{{ $post->title }}</strong></h2>
                        </div>
                        <div class="ms-auto p-2">
                            <h2>Author: <strong>{{ $post->user->name }}</strong></h2>
                            <h2>Published Date: <strong>{{ $post->published_at }}</strong></h2>
                        </div>
                    </div>
                    <img src="{{ asset('storage/' . $post->image) }}" alt="Post Image" height="300px" width="750px">
                    <p class="mt-2">{{ $post->content }}</p>
                </div>
            </div>
        </div>
    </div>

    <!-- SweetAlert CDN -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <script>
        function confirmDelete() {

            Swal.fire({
                title: 'Are you sure?',
                text: 'You won\'t be able to revert this!',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {

                    document.getElementById('deleteForm').submit();

                    Swal.fire(
                        'Deleted!',
                        'Your record has been deleted.',
                        'success'
                    );
                }
            });
        }
    </script>
</x-app-layout>
