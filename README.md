# Laravel Project Setup

This repository contains the source code for a Laravel project. Below are the steps to set up and run the project locally.

## Prerequisites

Before you begin, ensure you have the following installed on your machine:

- [PHP](https://www.php.net/)
- [Composer](https://getcomposer.org/)
- [Node.js](https://nodejs.org/) (Optional, for front-end assets)
- [Git](https://git-scm.com/)

## Getting Started

1. **Clone the Repository:**

    ```bash
    git clone https://gitlab.com/alaminse/wiztecbd-exam.git
    cd your-project
    ```

2. **Install Dependencies:**

    ```bash
    composer install
    ```

3. **Configure Environment:**

    - Copy the `.env.example` file to a new file called `.env`:

        ```bash
        cp .env.example .env
        ```

    - Open the `.env` file and configure your database connection and other settings.

4. **Generate Application Key:**

    ```bash
    php artisan key:generate
    ```

5. **Database Migration:**

    Run migrations to create tables in the database:

    ```bash
    php artisan migrate
    ```
6. **Database Migration:**

    Link Storage for file:

    ```bash
    php artisan storage:link
    ```

7. **Serve Your Application:**

    Start the development server:

    ```bash
    php artisan serve
    ```

    Your Laravel application should be accessible at `http://localhost:8000` by default.

## Additional Steps

- You might want to set up your version control system (Git) and commit your initial project files.
- Explore and modify the `resources/views` directory for your HTML views.
- Set up controllers, models, and routes based on your application requirements.

## Contributing

If you would like to contribute to this project, please follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and submit a pull request.

## License

This project is licensed under the [MIT License](LICENSE).
